import React from 'react';
import { Card, Text } from 'native-base';
import ThemesWrapper from '../../themes/ThemesWrapper';


const Button = ThemesWrapper(({ title, styles }) => {
  const { CardStyle, TextStyle } = styles;
  
  return (
      <Card style={CardStyle} >
          <Text style={TextStyle}> {title} </Text>
      </Card>
  );
}, 'Button');

export { Button };
export default Button;
