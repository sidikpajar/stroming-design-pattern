const config = {
  backgroundColor: '#017B8A',
  buttonColor: '#FB8A26',
  openSans: 'open-sans',
  putih:'white',
};

export default {
  Button: {
    TextStyle: {
      paddingLeft: 10,
      fontSize: 14,
      padding: 15,
      fontWeight: 'bold',
      fontFamily: config.openSans,
      color: config.putih,
    },
    CardStyle: {
      marginTop: 10,
      borderRadius: 50,
      marginRight: 50,
      marginLeft: 50,
      justifyContent: 'center',
      alignItems: 'center',
      height: 46.73,
      backgroundColor: config.buttonColor,
    },
  },


};
