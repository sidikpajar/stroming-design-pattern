import React, { Component } from 'react';
import defaultStyles from './default';

const ThemesWrapper = (WrappedComponent, name) => {
  class HOC extends Component {
    render() {

      const styles = typeof this.props.styles === 'object'
        ? this.props.styles
        : defaultStyles[name];

      return (
        <WrappedComponent
          { ...this.props }
          styles={ styles }
        />
      );
    };
  }

  return HOC;
};

export default ThemesWrapper;
